import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import {
    getDatabase,
    push,
    ref,
    onValue
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-database.js";
const firebaseConfig = {
    apiKey: "AIzaSyAAl3k7UFcCXqnqzUK2nSvSsOI8uYcw8_w",
    authDomain: "test-3f2f3.firebaseapp.com",
    databaseURL: "https://test-3f2f3-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "test-3f2f3",
    storageBucket: "test-3f2f3.appspot.com",
    messagingSenderId: "469891819570",
    appId: "1:469891819570:web:4c37c2112fe53f9a2c342b",
    measurementId: "G-4PCYM7J4C8"
};

const app = initializeApp(firebaseConfig);
//uruchumienie modłu RealtimeDatabase
const database = getDatabase(app)
// Definiowanie elementów UI
const nameInput = document.querySelector("#nameInput");
const messageInput = document.querySelector("#messageInput");
const sendMessageBtn = document.querySelector("#sendMessageBtn");
const chatArea = document.querySelector("#chatArea");

//1. Dodadanie możliwości wysyłania wiadomości
const sendMessage = () => {
    const authorName = nameInput.value;
    const messageName = messageInput.value;

    //1.3 zapisanie imienia oraz wiadomości w bazie danych
    push(ref(database, 'messages'), {
        author: authorName,
        message: messageName
    });

};
sendMessageBtn.addEventListener("click", sendMessage);

// 2. Dodanie możliwości odczytu wiadomości
// 2.1. Dodanie nasłuchiwania na zmiany pod ścieżką 'messages'
// onValue(na co ma nasłuchiwać?, co robić jak coś się zmieni?)
onValue(ref(database, 'messages'), (snapshot) => {
    // 2.2. Wygenerować widok HTML ze wszystkimi wiadomościami
    let chatContent = '';

    Object.values(snapshot.val()).forEach(message => {
        chatContent += `${message.author}: ${message.message}<br>`;
    });

    // 2.3. Umieszczenie wiadomości na stronie
    chatArea.innerHTML = chatContent;
});