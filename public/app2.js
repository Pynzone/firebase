import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import {
    getAuth,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    AuthErrorCodes,
    GoogleAuthProvider,
    signInWithPopup
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js";

import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    addDoc,
    collection,
    query,
    getDocs,
    where,
    orderBy,
    limit,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

const firebaseConfig = {
    apiKey: "AIzaSyAAl3k7UFcCXqnqzUK2nSvSsOI8uYcw8_w",
    authDomain: "test-3f2f3.firebaseapp.com",
    projectId: "test-3f2f3",
    storageBucket: "test-3f2f3.appspot.com",
    messagingSenderId: "469891819570",
    appId: "1:469891819570:web:4c37c2112fe53f9a2c342b",
    measurementId: "G-4PCYM7J4C8"
};
// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchmienie modułu uwierzytelniania
const auth = getAuth(app);
//uruchomienie modułu firestore
const database = getFirestore(app);
// Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const signUpBtn = document.querySelector("#signUpBtn");
const signInBtn = document.querySelector("#signInBtn");
const singInGoogleBtn = document.querySelector("#signInWithGoogleBtn");
const signOutBtn = document.querySelector("#signOutBtn");
const errorLabel = document.querySelector("#errorLabel");
const viewForNotLoggedUser = document.querySelector("#viewForNotLoggedUser");
const viewForLoggedUser = document.querySelector("#viewForLoggedUser");
const allAnimalsDiv = document.querySelector("#allAnimals");
const animalNameForm = document.querySelector("#animalNameForm");
const createAnimalBtn = document.querySelector("#createAnimalBtn");
// OPERACJA 1: Logowanie użytkownika
const signInUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    try {
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);
        console.log('User logged in.');
    } catch {
        // Ewentualnie, gdyby skorzystać z catch(exception) to możemy wyjąć
        // później kod błędu (poprzez exception.code)
        // console.log(exception.code);
        errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
    }
}
// Dodanie eventu do przycisku SignIn (wywołanie funkcji logoującej użytkownika)
signInBtn.addEventListener("click", signInUser);
// OPERACJA 2: Rejestracja użytkownika
const signUpUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    try {
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);

        console.log(user);
    } catch (error) {
        if (error.code === "auth/email-already-in-use") {
            console.log("wybierz inny email, ten jest zajęty..")
        } else {
            console.log("Rejestracja nie powiodła się...");
        }
    }
    // :warning: WAŻNE :warning:
    // wskaźnik -> doc(database, "uzytkownicy", user.user.uid)
    // Dodanie danych do bazy danych (np. Firestore)
};
// Dodanie eventu do przycisku SignUp (wywołanie funkcji rejrestrującej użytkownika)
signUpBtn.addEventListener("click", signUpUser);

// OPERACJA 3: Wylogowanie użytkownika
const signOutUser = async () => {
    await signOut(auth);
    console.log("User logged out.");
};
// Dodanie eventu to przycisku SignOut (wywołanie funkcji wylogowującej)
signOutBtn.addEventListener("click", signOutUser);
const generateAllAnimalsView = async () => {
    //zadanie 10
    const allAnimalsQuery = query(collection(database, 'Zwierzęta'));
    const allAnimals = await getDocs(allAnimalsQuery);
    let allAnimalsView = "<ul>";
    allAnimals.forEach(animal => {
        allAnimalsView += `<li>${animal.data().Nazwa}</li>`
    });

    allAnimalsView += "</ul>"
    allAnimalsDiv.innerHTML = allAnimalsView;
};

//zadanie 11
const createAnimal = async () => {
    const name = animalNameForm.value;
    const animalCollectionRef = collection(database, "Zwierzęta");

    await addDoc(animalCollectionRef, { Nazwa: name })

    generateAllAnimalsView();
};

createAnimalBtn.addEventListener("click", createAnimal);

//zadanie 12
const signInWithGoogle = () => {
    const AuthProvider = new GoogleAuthProvider();
    signInWithPopup(auth, AuthProvider);
};

singInGoogleBtn.addEventListener("click", signInWithGoogle);

// OPERACJA 4: Nasłuchiwanie na zmianę statusu sesji użytkownika

const authUserObserver = () => {
    onAuthStateChanged(auth, async (user) => {
        if (user) {
            // Ktoś jest zalogowany..
            viewForLoggedUser.style.display = 'block';
            viewForNotLoggedUser.style.display = 'none';

            generateAllAnimalsView();


        } else {
            // Ktoś nie jest zalogowany..
            viewForLoggedUser.style.display = 'none';
            viewForNotLoggedUser.style.display = 'block';
        }
    });
}
authUserObserver();

