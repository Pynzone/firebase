// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    addDoc,
    collection,
    query,
    getDocs,
    where,
    orderBy,
    limit,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";
const firebaseConfig = {
    apiKey: "AIzaSyAAl3k7UFcCXqnqzUK2nSvSsOI8uYcw8_w",
    authDomain: "test-3f2f3.firebaseapp.com",
    projectId: "test-3f2f3",
    storageBucket: "test-3f2f3.appspot.com",
    messagingSenderId: "469891819570",
    appId: "1:469891819570:web:4c37c2112fe53f9a2c342b",
    measurementId: "G-4PCYM7J4C8"
};
// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Firestore (baza danych)
const database = getFirestore(app);

const docRef = doc(database, "Zwierzęta", "1");
const docSnap = await getDoc(docRef);

console.log(docSnap.data().Nazwa);

const querySnapshot = await getDocs(collection(database, "Zwierzęta"));
querySnapshot.forEach((doc) => {
    // doc.data() is never undefined for query doc snapshots
    console.log(doc.id, " => ", doc.data());
});

// Add a new document with a generated id.
// const docRef1 = await addDoc(collection(database, "Zwierzęta"), {
//     Nazwa: 'Czita',
//     Cena: 900,
//     Wymagane_zezwolenie: false
// });
// console.log("Document written with ID: ", docRef1.id);
