import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

import {
    getStorage,
    uploadBytesResumable,
    ref,
    getDownloadURL,
    listAll
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyAAl3k7UFcCXqnqzUK2nSvSsOI8uYcw8_w",
    authDomain: "test-3f2f3.firebaseapp.com",
    projectId: "test-3f2f3",
    storageBucket: "test-3f2f3.appspot.com",
    messagingSenderId: "469891819570",
    appId: "1:469891819570:web:4c37c2112fe53f9a2c342b",
    measurementId: "G-4PCYM7J4C8"
};

//--------------------------Wrzucanie PLIKU Z DYSKU NA SERWER--------------------------------------------

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Storage
const storage = getStorage(app);

// Definiowanie elementów UI
const imageInput = document.querySelector("#imageInput");
const imageName = document.querySelector("#imageName");
const uploadImageBtn = document.querySelector("#uploadButton");
const uploadImageProgressBar = document.querySelector("#uploadImageProgressBar");
const uploadedImage = document.querySelector("#uploadedImage");
const ImagesList = document.querySelector("#ImagesList");

// ----- WRZUCANIE PLIKU Z DYSKU LOKALNEGO NA SERWER -----
// 1. Zmienne pomocnicze do przechowania pliku
let fileToUpload;
let fileExtension;

// 2. Obsługa procesu wyboru pliku
// 2.1. Wbijamy się w moment, kiedy plik zostanie wybrany,
//      więc event 'change' zostanie wywołany
imageInput.onchange = (event) => {




    // 2.3. Pobranie pełnej nazwy wybranego przez użytkownika pliku
    const fullFileName = event.target.files[0].name;

    // 2.4. Podział pełnej nazwy wybranego pliku na nazwę oraz rozszerzenie pliku
    // divi_moduły.jpg -> ["nazwa pliku", "jpg"]
    const splittedFileName = fullFileName.split('.');

    console.log(splittedFileName.length)
    if (splittedFileName.length > 2) {
        window.alert("Niepoprawna Nazwa pliku")
    } else {
        // 2.2. Zapamiętanie całego pliku, który wrzucimy na Storage
        fileToUpload = event.target.files[0];

        // 2.5. Pobranie samego rozszerzenia pliku
        fileExtension = splittedFileName[1];
    }


};

// 3. Przesyłanie wybranego obrazka do Firebase (Storage)
uploadImageBtn.onclick = () => {

    // 3.1. Pobranie nazwy docelowego pliku z inputa
    const fileName = imageName.value;

    // 3.2. Złączenie docelowej nazwy pliku z rozszerzeniem pliku
    // Plik musi mieć pełną nazwę, tzn. nazwa + rozszerzenie (np. obrazek.jpg)
    const fullFileName = `${fileName}.${fileExtension}`

    // 3.3. Określenie metadanych
    const metaData = {
        contentType: fileToUpload.type
    };

    // 3.4. Komunikacja ze Storage w celu wrzucenia pliku
    const uploadProcess = uploadBytesResumable(ref(storage, `images/${fullFileName}`), fileToUpload, metaData);

    // 3.5. Wbijamy się w moment, kiedy status wrzucania pliku będzie się zmieniał
    // Składania Callback -> Promise -> async/await
    uploadProcess.on("state_changed",
        // Co robić w trakcie przesyłania?
        (snapshot) => {
            const progress = snapshot.bytesTransferred / snapshot.totalBytes * 100;
            uploadImageProgressBar.innerHTML = `${Math.round(progress)}%`;
        },
        // Co robić kiedy błąd podczas przesyłania?
        (error) => {
            console.error(error);
        },
        // Co robić kiedy proces zakończy się sukcesem?
        async () => {

            // 3.6. Pobranie URL pliku
            const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);

            // 3.7. Umieszczanie obrazka na stronie
            uploadedImage.setAttribute("src", imageUrl);

        }
    );
};

// ----- ODCZYT PLIKÓW Z SERWERA -----

// 4. Pobranie konkretnego obrazka

// 4.1. Pobranie URL obrazka


// 4.2. Umieszczenie obrazka na stronie


// 5. Pobranie wszystkich dostępnych plików (w folderze)

// 5.1. Pobieranie referencji (wskażników) plików z konkretnego     folderu. Jako wynik otrzymamy tablicę
const allImages = await listAll(ref(storage, 'images'));
// 5.2. Przejście pętlą przez wszystkie znalezione pliki
let imagesListView = '<select>';
let imageUrlFromList;
allImages.items.forEach(async (imageRef) => {
    //const imageUrl = await getDownloadURL(imageRef);
    // Wyświetlanie linków do zdjęć -> console.log(imageUrl);
    imageUrlFromList = imageRef._location.path;
    // Wyświetlanie ścieżek zdjęć:
    imagesListView += `<option> ${imageUrlFromList}</option>`;
});
imagesListView += '</select>'
console.log(imageUrlFromList);
const imageUrl = await getDownloadURL(ref(storage, `${imageUrlFromList}`));
uploadedImage.setAttribute("src", imageUrl);
ImagesList.innerHTML = imagesListView;